FROM golang:alpine3.9 as builder

LABEL version="0.1"
LABEL description="A sample Go service for Reyn"
LABEL maintainer="azizo"

RUN mkdir /go/src/app && apk update && apk add git
WORKDIR /go/src/app
COPY app .
RUN go get && CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

ENV APP_NAME=Azizo
# ENV PORT=8001
# EXPOSE $PORT
CMD ["./app","-web.listen-address=0.0.0.0:8001"]