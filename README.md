# Reyn service

This project used to demonstrate the basic functionalities of Go and how it can be used to process and display real-time data using Web Sockets.

Each 5 seconds, the application generates a set of _unu_ and _dos_ float values. These values are outputted on the console and will not be shown on the HTML page. However, every 1 minutes (by default, but configurable) the front-end shows the sum of the first unos and the average of dos values generated previously. All real-time.

# Use

To run locally, you'll need to install Go.
However, it's cleaner to build and use your own Docker image using the Dockerfile in the root directory:

- `docker build -t reyn .`
- `docker run -p 8001:8001 reyn`
-  Visit `localhost:8001` on your browser!

Don't you want to build your own imeage, then you can use the latest image on the registy: 
- `docker run -p 8001:8001 registry.gitlab.com/azizoo/reyno:0.1`

# Note

- For simplicity reasons, the client (a simple HTML page) is being served by the same Go server as used for the service itself. Therefore, the call to the WebSocket service is hard-coded in the embedded Javascript part of HTML. It also means ignoring parameterizing and injecting any port value to the Dockerfile (to be used as env. Variable), because then the port may vary.

- Although the front-end/client is the only consumer of the WebSocket, you could still access the real-time data provided by the following endpoints: `ws://localhost:8001/dps` and `ws://localhost:8001/agg/1`

- The time range for aggregating and calculating data-points is configurable using the path variable to requests to `/agg/`. The value will represent the number (int) minutes the calculations should happen, e.g. `ws://localhost:8001/agg/2` for two minutes.