package models

// Producer is there for each single running instance
type Producer struct {
	APPName string `json:"appName"`
	Queue   *Queue `json:"queue"`
}

// Queue objects are containing the generated values
type Queue struct {
	UNO float64 `json:"uno"`
	DOS float64 `json:"dos"`
}

// State contains the state of aggregator
type State struct {
	SumUNO float64 `json:"uno"`
	AvgDos float64 `json:"dos"`
}
