package services

import (
	"app/common"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/joho/godotenv"
	"math"
	"math/rand"
	"os"
	"strconv"
	"time"
)

var unos []float64
var dos []float64
var stateValidityPerMinute int = 1

// DPSWebSocketWriter handles WebSocket requests for data points
func DPSWebSocketWriter(conn *websocket.Conn) {
	// The numbers of unos and dos is calculated based on the given time range to calculate values (sum+avg)
	// By default every 5 seconds new data gets generated and each minute the state restores (which is now configurable thanks to stateValidityPerMinute)
	queueValuesSize := stateValidityPerMinute * 60 / 5
	for {
		ticker := time.NewTicker(5 * time.Second)
		for t := range ticker.C {
			fmt.Printf("Retrieving new data.. %+v\n", t)
			producer := *ProduceDataPoint()
			jsonString, err := json.Marshal(producer)
			if len(unos) == queueValuesSize && len(dos) == queueValuesSize {
				unos = nil
				dos = nil
			}
			unos = append(unos, producer.Queue.UNO)
			dos = append(dos, producer.Queue.DOS)

			if err != nil {
				fmt.Println(err)
			}
			if err := conn.WriteMessage(websocket.TextMessage, []byte(jsonString)); err != nil {
				fmt.Println(err)
				return
			}
		}
	}
}

// AGGWebSocketWriter handles WebSocket requests for calculated data
func AGGWebSocketWriter(conn *websocket.Conn, inputDuration string) {
	stateValidityPerMinute, err := strconv.Atoi(inputDuration)
	if err != nil {
		fmt.Println(err)
	}
	for {
		// The AGG calculation happes every "stateValidityPerMinute" minute
		ticker := time.NewTicker(time.Duration(stateValidityPerMinute) * time.Minute)

		for t := range ticker.C {
			fmt.Printf("Calculated previous values.. %+v\n", t)
			state := *aggregator()
			jsonString, err := json.Marshal(state)
			if err != nil {
				fmt.Println(err)
			}
			if err := conn.WriteMessage(websocket.TextMessage, []byte(jsonString)); err != nil {
				fmt.Println(err)
				return
			}
		}
	}
}

// ProduceDataPoint generates random data point at random interval
func ProduceDataPoint() *models.Producer {
	godotenv.Load()

	queue := new(models.Queue)
	queue.UNO = math.Round(rand.Float64()*100) / 100
	queue.DOS = math.Round(rand.Float64()*100) / 100

	producer := new(models.Producer)

	producer.APPName = os.Getenv("APP_NAME")
	producer.Queue = queue

	return producer
}

// Aggregator generates random data point at random interval
func aggregator() *models.State {
	state := new(models.State)
	state.SumUNO = math.Round(sumArrayElements(unos)*100) / 100
	state.AvgDos = math.Round(avgArrayElements(dos)*100) / 100
	return state
}

// helper method to find sum of float64 elements in an array
func sumArrayElements(numbers []float64) float64 {
	result := 0.0
	for _, value := range numbers {
		result += value
	}
	return result
}

// helper method to find avg of float64 elements in an array
func avgArrayElements(numbers []float64) float64 {
	return sumArrayElements(numbers) / float64(len(numbers))
}
