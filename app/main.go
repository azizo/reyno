package main

import (
	"app/handlers"
	"fmt"
	"github.com/joho/godotenv"
	"log"
	"net/http"
	"os"
)

func main() {
	routes()
}

func routes() {
	godotenv.Load()
	//PORT := os.Getenv("PORT")
	PORT := "8001"
	APP := os.Getenv("APP_NAME")

	// Used to provide real-time generated data points (WebSocket)
	http.HandleFunc("/dps", handlers.DPSHandler)

	// Used to provide real-time generated sum and average values (WebSocket)
	http.HandleFunc("/agg/", handlers.AGGHandler)

	// Simple HTML rendering
	homePage := http.FileServer(http.Dir("static"))
	http.Handle("/", homePage)

	fmt.Printf("Application %v starting on port %v..\n", APP, PORT)

	log.Fatal(http.ListenAndServe("0.0.0.0:"+PORT, nil))
}
