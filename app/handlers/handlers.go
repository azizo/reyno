package handlers

import (
	"app/services"
	"fmt"
	"github.com/gorilla/websocket"
	"net/http"
	"strings"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// DPSHandler is responsible to provide WebSocket connection for live generating data points
func DPSHandler(w http.ResponseWriter, r *http.Request) {
	ws, err := Upgrade(w, r)
	if err != nil {
		fmt.Fprintf(w, "%+v\n", err)
	}
	go services.DPSWebSocketWriter(ws)
}

// AGGHandler is responsible to provide WebSocket connection for displaying live state
func AGGHandler(w http.ResponseWriter, r *http.Request) {
	inputDuration := strings.TrimPrefix(r.URL.Path, "/agg/")

	ws, err := Upgrade(w, r)
	if err != nil {
		fmt.Fprintf(w, "%+v\n", err)
	}
	go services.AGGWebSocketWriter(ws, inputDuration)
}

// Upgrade is to init the websocket connection (through HTTP and acting as a helper)
func Upgrade(w http.ResponseWriter, r *http.Request) (*websocket.Conn, error) {
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	ws, err := upgrader.Upgrade(w, r, nil)

	if err != nil {
		fmt.Println(err)
		return ws, err
	}
	return ws, err
}
